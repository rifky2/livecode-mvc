from django.contrib import admin
from .models import barang

class BarangAdmin(admin.ModelAdmin):
    list_display = ('nama', 'foto', 'deskripsi', 'harga')

admin.site.register(barang, BarangAdmin)