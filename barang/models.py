from django.db import models

class barang (models.Model):
    nama = models.CharField(max_length = 50)
    foto = models.CharField(max_length = 50)
    deskripsi = models.TextField()
    harga = models.CharField(max_length = 20)

    def __str__(self):
        return self.nama