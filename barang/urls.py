from django.urls import path
from . import views

urlpatterns = [
    path('', views.Barang, name='barang'),
    path('barang/<int:id>', views.barang_detail, name='barang')
]