from django.shortcuts import render
from .models import barang
from django.http import HttpResponse

# def Barang(request):
    # return HttpResponse('akubarang')

def Barang (request):
    list_barang = barang.objects.all()
    return render(request, 'barang/barang.html', {'list_barang' : list_barang})

def barang_detail (request, id):
    detail_barang = barang.objects.get(pk=id)
    return render(request, 'barang/barang_detail.html', {'detail_barang' : detail_barang})